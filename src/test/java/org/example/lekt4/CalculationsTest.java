package org.example.lekt4;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculationsTest {

    @Test
    public void add(){
        // Given
        int a = 5;
        int b = 7;
        Calculator calculator = new Calculator();

        // When
        int sum = calculator.add(a, b);

        // Then
        assertEquals(12, sum);
    }

}
