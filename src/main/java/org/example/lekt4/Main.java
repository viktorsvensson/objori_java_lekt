package org.example.lekt4;

import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        /*
        int x = 5;
        int y = 2;
        y = x * y;

        Car car = new Car("Red");
        car = changeCar(car);
        System.out.println(car.getColor());

        List<String> names = List.of("Anna", "Berit", "Calle");

        System.out.println(names);
        */

        Scanner scanner = new Scanner(System.in);
        System.out.println("Hej, vad heter du?");
        String fname = scanner.nextLine();
        System.out.println(fname + ", vilket fint namn!");
        System.out.println("Vad är din ålder?");
        int age = scanner.nextInt();
        System.out.println("Okej, din åler är " + age);
        System.out.println("Vad är din epost-adreess?");
        String emailPattern = "\\w+@\\w+\\.\\w+";
        while(!scanner.hasNext(emailPattern)){
            System.out.println("Ogiltig epost, försök igen");
            scanner.nextLine();
        }
        String email = scanner.next(emailPattern);
        System.out.println("Skickar bekräftelse till " + email);


    }

    private static int sum(int x, int y){
        return x + y;
    }

    private static Car changeCar(Car localCar){
        localCar.setColor("Green");
        localCar = new Car("Blue");
        localCar.setColor("Yellow");
        return localCar;
    }

}
