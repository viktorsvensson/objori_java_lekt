package org.example.lekt5;

import java.util.List;

public interface RoomBookingRepo {

    List<RoomBooking> getRoomBookingsByRoomId(int roomId);

    RoomBooking add(RoomBooking roomBooking);

}
