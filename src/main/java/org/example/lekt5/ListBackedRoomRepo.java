package org.example.lekt5;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class ListBackedRoomRepo implements RoomRepo {

    private final static List<Room> roomList = new ArrayList<>(List.of(
            new Room(1, 101, 2, 899),
            new Room(2, 102, 2, 1399),
            new Room(3, 201, 4, 2999)
    ));

    @Override
    public Room getById(int id) {
        for(Room room : roomList){
            if(room.getRoomId() == id){
                return room;
            }
        }
        throw new NoSuchElementException("Room doesn't exist");
    }
}
