package org.example.lekt5;

import java.time.LocalDate;
import java.time.Period;

public class RoomBooking {

    private Room room;
    private String guestName;
    private int numberOfGuests;
    private LocalDate startDate;
    private LocalDate endDate;

    public RoomBooking(Room room, String guestName, int numberOfGuests, LocalDate startDate, LocalDate endDate) {
        this.room = room;
        this.guestName = guestName;
        this.numberOfGuests = numberOfGuests;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Room getRoom() {
        return room;
    }

    public String getGuestName() {
        return guestName;
    }

    public int getNumberOfGuests() {
        return numberOfGuests;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public int getTotalPrice(){
        return room.getCostPerDay() * Period.between(startDate, endDate).getDays();
    }

    @Override
    public String toString() {
        return "RoomBooking{" +
                "room=" + room.getRoomId() +
                ", guestName='" + guestName + '\'' +
                ", numberOfGuests=" + numberOfGuests +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", totalPrice=" + getTotalPrice() +
                '}';
    }
}
