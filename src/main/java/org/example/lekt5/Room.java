package org.example.lekt5;

public class Room {

    private int roomId;
    private int roomNumer;
    private int numberOfBeds;
    private int costPerDay;

    public Room(int roomId, int roomNumer, int numberOfBeds, int costPerDay) {
        this.roomId = roomId;
        this.roomNumer = roomNumer;
        this.numberOfBeds = numberOfBeds;
        this.costPerDay = costPerDay;
    }

    public int getRoomId() {
        return roomId;
    }

    public int getRoomNumer() {
        return roomNumer;
    }

    public int getCostPerDay() {
        return costPerDay;
    }

    @Override
    public String toString() {
        return "Room{" +
                "roomId=" + roomId +
                ", roomNumer=" + roomNumer +
                ", numberOfBeds=" + numberOfBeds +
                ", costPerDay=" + costPerDay +
                '}';
    }
}
