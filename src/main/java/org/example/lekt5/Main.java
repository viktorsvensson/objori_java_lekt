package org.example.lekt5;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Main {

    // id, roomNumber, beds, CostPerDay
    private static final RoomRepo roomRepo = new ListBackedRoomRepo();

    // roomId, guestName, numberOfGuests, startDate, endDate
    private static final RoomBookingRepo roomBookingRepo = new ListBackedRoomBookingRepo();

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Startar hotellprogram 3000 ...");
        int choice;
        do {
            choice = displayAndSelectFromMainMenu();
            if (choice == 1) {
                displayAndSelectFromBookingMenu();
            }
            if (choice == 2) {
                System.out.println("Vilket rumm vill du visa bokningar för?");
                int roomId = scanner.nextInt();
                scanner.nextLine();
                showBookingsForRoomId(roomId);
            }

        } while (choice != 0);
    }

    private static void showBookingsForRoomId(int roomId) {
        System.out.println("Bokade tillfällen just nu:");
        for (RoomBooking roomBooking : roomBookingRepo.getRoomBookingsByRoomId(roomId)) {
            System.out.println(roomBooking);
        }
    }

    private static void displayAndSelectFromBookingMenu() {
        System.out.println("Vilket rum vill du boka?");
        int roomId = scanner.nextInt();
        scanner.nextLine();
        Room room = roomRepo.getById(roomId);
        System.out.println("Okej, du vill göra en bokning av rummet " + room);

        System.out.println("I vilket namn vill du boka rummet?");
        String guestName = scanner.nextLine();
        System.out.println("Hur många gäster ska det vara?");
        int numberOfGuests = scanner.nextInt();
        scanner.nextLine();
        System.out.println("Vilket datum vill du starta? (yyyy-mm-dd)");
        LocalDate startDate = LocalDate.parse(scanner.nextLine(), DateTimeFormatter.ISO_LOCAL_DATE);
        System.out.println("Vilket datum vill du avsluta? (yyyy-mm-dd");
        LocalDate endDate = LocalDate.parse(scanner.nextLine(), DateTimeFormatter.ISO_LOCAL_DATE);
        roomBookingRepo.add(new RoomBooking(room, guestName, numberOfGuests, startDate, endDate));
        System.out.println("Rumsbokning skapad: " + room);

    }

    private static int displayAndSelectFromMainMenu() {
        System.out.println("1. Boka ett rum");
        System.out.println("2. Visa bokningar");
        System.out.println("0. Avsluta programmet");
        return scanner.nextInt();
    }

}
