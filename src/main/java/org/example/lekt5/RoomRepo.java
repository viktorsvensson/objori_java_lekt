package org.example.lekt5;

public interface RoomRepo {
    Room getById(int id);
}
