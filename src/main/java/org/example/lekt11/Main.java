package org.example.lekt11;

import java.util.Comparator;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        // 1.1 - Skapa en lista på 10 siffror
        List<Integer> numbers = List.of(1, 2, 2, 643, 135, 7, 9, 13, 124, 353);

        // 1.2 - Plocka ut siffror större än 3 till en ny lista
        List<Integer> numbersLargerThanThree = numbers.stream()
                .filter(x -> x > 3)
                .toList();

        System.out.println(numbersLargerThanThree);
        System.out.println("------");

        // 1.3 - Multiplicera alla talen i listan med 3 till en ny lista
        List<Integer> numbersMultipliedByThree = numbers.stream()
                .map(x -> x * 3)
                .toList();

        System.out.println(numbersMultipliedByThree);
        System.out.println("------");

        // 1.4 Sortera alla talen i listan till en ny lista
        List<Integer> sortedNumbers = numbers
                .stream()
                .sorted()
                .toList();

        System.out.println(sortedNumbers);
        System.out.println("------");

        // 1.5 - Skapa en stream av listan,
        // filtrera bort alla värden över 10,
        // multiplicera de som är kvar med 5
        // sortera från höst till lägst
        // plocka bort ev. dubletter
        // skriv ut de 3 högsta kvarvarande värdena
        List<Integer> treatedNumbers = numbers.stream()
                .filter(x -> x <= 10)
                .map(x -> x * 5)
                .sorted(Comparator.reverseOrder())
                .distinct()
                .limit(3)
                .toList();

        System.out.println(treatedNumbers);
        System.out.println("-------");

        record Employee(String name, int salary, String gender){}

        List<Employee> employees = List.of(
                new Employee("Alice", 27560, "Female"),
                new Employee("Bob", 35000, "Male"),
                new Employee("Charlie", 37000, "Non-binary"),
                new Employee("David", 51000, "Male"),
                new Employee("Erin", 52000, "Female"),
                new Employee("Frank", 32000, "Male"),
                new Employee("Grace", 49000, "Female")
        );

        // 2.2 Plocka ut en lista med tredje, fjärde och femte mest välbetalde anställde
        System.out.println(
            employees.stream()
                    .sorted(Comparator.comparing(Employee::salary).reversed())
                    .skip(2)
                    .limit(3)
                    .toList()
        );
        System.out.println("-------");

        // 2.3 Hitta den kvinna som har näst högst lön
        System.out.println(
                employees.stream()
                        .filter(emp -> emp.gender().equals("Female"))
                        .sorted(Comparator.comparing(Employee::salary).reversed())
                        .skip(1)
                        .findFirst()
                        .orElseThrow()
        );
        System.out.println("----");

        // 2.4 räkna antalet olika könsidentiteter som finns bland de anställda
        System.out.println(
                employees.stream()
                        .map(Employee::gender)
                        .distinct()
                        .count()
        );
        System.out.println("------");

        // 2.5 Returnera sant eller falskt, beroende på om någon av de 3 högst betalda är kvinnor
        System.out.println(
                employees.stream()
                        .sorted(Comparator.comparing(Employee::salary).reversed())
                        .limit(3)
                        .anyMatch(emp -> emp.gender().equals("Female"))
        );



    }

}
