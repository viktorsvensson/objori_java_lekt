package org.example.lekt6.roombooking;

import java.util.List;

public interface RoomBookingRepo {

    List<RoomBooking> getRoomBookingsByRoomId(int roomId);

    RoomBooking add(RoomBooking roomBooking);

}
