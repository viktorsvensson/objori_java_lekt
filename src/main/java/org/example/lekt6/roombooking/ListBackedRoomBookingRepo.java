package org.example.lekt6.roombooking;

import java.util.ArrayList;
import java.util.List;

public class ListBackedRoomBookingRepo implements RoomBookingRepo {

    private static final List<RoomBooking> roomBookings = new ArrayList<>();

    @Override
    public List<RoomBooking> getRoomBookingsByRoomId(int roomId) {
        List<RoomBooking> returnList = new ArrayList<>();
        for(RoomBooking roomBooking : roomBookings){
            if(roomBooking.getRoom().getRoomId() == roomId){
                returnList.add(roomBooking);
            }
        }
        return returnList;

    }

    @Override
    public RoomBooking add(RoomBooking roomBooking) {
        roomBookings.add(roomBooking);
        return roomBooking;
    }
}
