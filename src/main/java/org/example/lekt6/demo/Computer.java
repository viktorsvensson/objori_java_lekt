package org.example.lekt6.demo;

public class Computer {

    private static final Computer computer = new Computer();

    private Computer(){
    }

    public static Computer getInstance(){
        return computer;
    }

}
