package org.example.lekt6.menu;

public interface MenuState {

    void execute();

}
