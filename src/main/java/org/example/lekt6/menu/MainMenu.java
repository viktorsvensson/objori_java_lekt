package org.example.lekt6.menu;

import org.example.lekt6.room.ListBackedRoomRepo;
import org.example.lekt6.room.Room;
import org.example.lekt6.room.RoomRepo;
import org.example.lekt6.roombooking.ListBackedRoomBookingRepo;
import org.example.lekt6.roombooking.RoomBooking;
import org.example.lekt6.roombooking.RoomBookingRepo;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;

public class MainMenu extends Menu implements MenuState {

    private static final RoomRepo roomRepo = new ListBackedRoomRepo();
    private static final RoomBookingRepo roomBookingRepo = new ListBackedRoomBookingRepo();
    private static final Scanner scanner = new Scanner(System.in);

    public MainMenu() {
        super("Main Menu");
        menuOptions = List.of(
                new MenuOption(1, "Boka ett rum", () -> makeABooking()),
                new MenuOption(2, "Visa bokningsinfo",
                        () -> MenuSystem.setState(new BookingInfoMenu())),
                new MenuOption(3, "Quit", () -> System.exit(0))
        );
    }

    private static void makeABooking() {
        System.out.println("Vilket rum vill du boka?");
        int roomId = scanner.nextInt();
        scanner.nextLine();
        Room room = roomRepo.getById(roomId);
        System.out.println("Okej, du vill göra en bokning av rummet " + room);

        System.out.println("I vilket namn vill du boka rummet?");
        String guestName = scanner.nextLine();
        System.out.println("Hur många gäster ska det vara?");
        int numberOfGuests = scanner.nextInt();
        scanner.nextLine();
        System.out.println("Vilket datum vill du starta? (yyyy-mm-dd)");
        LocalDate startDate = LocalDate.parse(scanner.nextLine(), DateTimeFormatter.ISO_LOCAL_DATE);
        System.out.println("Vilket datum vill du avsluta? (yyyy-mm-dd");
        LocalDate endDate = LocalDate.parse(scanner.nextLine(), DateTimeFormatter.ISO_LOCAL_DATE);
        roomBookingRepo.add(new RoomBooking(room, guestName, numberOfGuests, startDate, endDate));
        System.out.println("Rumsbokning skapad: " + room);

    }

}
