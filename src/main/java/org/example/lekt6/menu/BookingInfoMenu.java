package org.example.lekt6.menu;

import org.example.lekt6.roombooking.ListBackedRoomBookingRepo;
import org.example.lekt6.roombooking.RoomBooking;
import org.example.lekt6.roombooking.RoomBookingRepo;

import java.util.List;
import java.util.Scanner;

public class BookingInfoMenu extends Menu {

    private static final RoomBookingRepo roomBookingRepo = new ListBackedRoomBookingRepo();
    private static final Scanner scanner = new Scanner(System.in);

    public BookingInfoMenu() {
        super("Bokningsinformation");
        menuOptions = List.of(
                new MenuOption(1, "Visa bokningar för specifikt rum", () -> showBookingsForRoomId()),
                new MenuOption(2, "Gå till huvudmenyn", () -> MenuSystem.setState(new MainMenu()))
        );
    }

    private static void showBookingsForRoomId() {
        System.out.println("Vilket rum vill du visa bokningar för?");
        int roomId = scanner.nextInt();
        scanner.nextLine();
        System.out.println("Bokade tillfällen just nu:");
        for (RoomBooking roomBooking : roomBookingRepo.getRoomBookingsByRoomId(roomId)) {
            System.out.println(roomBooking);
        }
    }
}
