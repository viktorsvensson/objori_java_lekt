package org.example.lekt6.room;



import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class ListBackedRoomRepo implements RoomRepo {

    private final static List<Room> roomList = new ArrayList<>(List.of(
            new Room.RoomBuilder(1)
                    .roomNumber(101)
                    .numberOfBeds(2)
                    .costPerDay(899)
                    .build(),
            new Room.RoomBuilder(2)
                    .roomNumber(102)
                    .numberOfBeds(2)
                    .costPerDay(1399)
                    .build(),
            new Room.RoomBuilder(3)
                    .roomNumber(201)
                    .numberOfBeds(2)
                    .costPerDay(2999)
                    .build()
    ));

    @Override
    public Room getById(int id) {
        for(Room room : roomList){
            if(room.getRoomId() == id){
                return room;
            }
        }
        throw new NoSuchElementException("Room doesn't exist");
    }
}
