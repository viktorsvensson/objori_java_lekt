package org.example.lekt6.room;

public interface RoomRepo {
    Room getById(int id);
}
