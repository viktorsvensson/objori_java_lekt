package org.example.lekt6.room;

public class Room {

    private int roomId;
    private int roomNumer;
    private int numberOfBeds;
    private int costPerDay;

    private Room(int roomId, int roomNumer, int numberOfBeds, int costPerDay) {
        this.roomId = roomId;
        this.roomNumer = roomNumer;
        this.numberOfBeds = numberOfBeds;
        this.costPerDay = costPerDay;
    }

    public static class RoomBuilder {

        private int roomId;
        private int roomNumber;
        private int numberOfBeds;
        private int costPerDay;

        public RoomBuilder(int roomId){
            this.roomId = roomId;
        }

        public RoomBuilder roomNumber(int roomNumber){
            this.roomNumber = roomNumber;
            return this;
        }

        public RoomBuilder numberOfBeds(int numberOfBeds){
            this.numberOfBeds = numberOfBeds;
            return this;
        }

        public RoomBuilder costPerDay(int costPerDay){
            this.costPerDay = costPerDay;
            return this;
        }

        public Room build(){
            return new Room(roomId, roomNumber, numberOfBeds, costPerDay);
        }


    }

    public int getRoomId() {
        return roomId;
    }

    public int getRoomNumer() {
        return roomNumer;
    }

    public int getCostPerDay() {
        return costPerDay;
    }

    @Override
    public String toString() {
        return "Room{" +
                "roomId=" + roomId +
                ", roomNumer=" + roomNumer +
                ", numberOfBeds=" + numberOfBeds +
                ", costPerDay=" + costPerDay +
                '}';
    }
}
