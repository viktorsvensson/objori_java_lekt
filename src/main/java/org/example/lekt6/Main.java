package org.example.lekt6;

import org.example.lekt6.menu.MenuSystem;

public class Main {

    public static void main(String[] args) {

        System.out.println("Startar hotellprogram 3000 ...");

        MenuSystem menuSystem = MenuSystem.getInstance();
        while (true) {
            menuSystem.execute();
        }

    }

}
