package org.example.lekt2;

public class Staff {

    private final int id;

    public Staff(int id){
        this.id = id;
    }

    public void quit(){
        System.out.println("Hejdå, nu säger jag upp mig");
    }

    protected int getId() {
        return id;
    }
}
