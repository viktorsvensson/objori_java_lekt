package org.example.lekt2;

public class Employee extends Staff {

    private int salary;

    public Employee(int id, int salary) {
        super(id);
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getId(){
        return super.getId();
    }
}
