package org.example.lekt2;

public class Car {

    private String color;
    static private int counter = 0;

    public Car(String color){
        this.color = color;
        counter++;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public static int getCounter() {
        return counter;
    }

    public static void setCounter(int counter) {
        Car.counter = counter;
    }
}
