package org.example.lekt2;

import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        /*
        Car car1 = new Car("Green");
        Car car2 = new Car("Blue");

        car1.setColor("Orange");
        System.out.println(car1.getColor());
        System.out.println(Car.getCounter());

        Employee employee1 = new Employee(1, 34000);
        Staff employee2 = new Employee(2, 29000);
        Intern intern1 = new Intern(3);

        List<Staff> staffList = List.of(employee1, intern1);
        staffList.forEach(staff -> staff.quit());
        */

        int[] numbers = {1, 2, 3, 4, 5};

        String[] names = {"Anna", "Berit", "Cecilia"};
        System.out.println(names);

        for(int i = 0; i < numbers.length; i++){
            if(numbers[i] % 2 == 0) {
                System.out.println("Talet " + numbers[i] + " är jämnt");
            } else {
                System.out.println("Talet " + numbers[i] + " är udda");
            }
        }

        Scanner scanner = new Scanner(System.in);
        String answer = "";
        while(!answer.equals("Oslo")){
            System.out.println("Vad heter Norges huvudstad?");
            answer = scanner.nextLine();
        }



    }

}
