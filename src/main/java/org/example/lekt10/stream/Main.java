package org.example.lekt10.stream;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {

        // Stream.of(val1, val2, val3) - skapar en ström av argumenten
        // .forEach(Consumer) - Konsumerar elementen (t.ex. skriver ut dem i konsol)
        Stream.of(1, 2, 3, 4)
                .forEach(val -> System.out.println(val));

        Stream.of("Alice", "Bob", "Charlie", "Gunnar")
                .forEach(System.out::println);

        Stream.of(List.of("Hej", "Då"), List.of(1, 2))
                .forEach(System.out::println);

        // .stream() från lista -> lista till ström av dess element
        // .filter(Predicate) -> släpper bara igenom element som uppfyller predikatets villkor
        // .toList() -> terminal / avslutande operation som returnerar en lista av elementen som når den
        List<Integer> filteredNumbers = List.of(1, 2, 3, 4)
                .stream()
                .filter(x -> x > 2)
                .toList();

        System.out.println(filteredNumbers);

        // .map(Function) -> transformerar ett värde, tar in ett argument och ändrar det på något sätt
        // .peek(Consumer) -> låter oss konsumera (typiskt printa ut) ett värde för debugging, innan sänder vidare
        List<String> mappedNames = Stream.of("Alice", "Bob", "Charlie")
                //.peek(str -> System.out.println("Före map: " + str))
                .map(name -> name.substring(0, 3))
                //.peek(str -> System.out.println("Efter map: " + str ))
                .toList();

        System.out.println(mappedNames);

        // .sorted(Comparator) -> sorterar vår lista, stateful
        int firstEvenNumber = List.of(1, 2, 3, 4, 5, 6, 7, 8)
                .stream()
                .filter(x -> x % 2 == 0)
                .sorted(Comparator.reverseOrder())
                .findFirst().orElseThrow();

        System.out.println(firstEvenNumber);

        int intFirstEvenNumber2 = List.of(1, 2, 3, 4)
                .stream()
                .filter(x -> x % 2 == 0)
                .findFirst().orElseThrow();

        System.out.println(intFirstEvenNumber2);

        System.out.println("-------");

        // limit(n) -> begränsar till de n första elementen
        // skip(n) -> skippar de n första elementen, släpper igenom resten
        // distinct() -> tar bort multipler av elementen, t.ex. 3, 3, 2, 3 -> 3, 2
        // count() -> räknar antalet element som når count()
        long numbersLeft = List.of(1, 2, 3, 3, 3, 4, 5, 6)
                .stream()
                .limit(5)
                .skip(2)
                .distinct()
                .count();

        System.out.println(numbersLeft);

        // noneMatch(Predicate) -> inget element uppfyller villkoret ger true
        // anyMatch(Predicate) -> något av elementen uppfyller villkoren ger true
        // allMatch(Predicate) -> alla element uppfyller villkoren ger true
        boolean result = List.of("Pizza", "Pasta", "Hamburgare")
                .stream()
                .noneMatch(word -> word.startsWith("P"));

        System.out.println("-------");
        System.out.println(result);


    }

}
