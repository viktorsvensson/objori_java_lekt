package org.example.lekt10.ovnlos;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class Main {

    public static void main(String[] args) {
        //uppg1("Hejsan");
        uppg2();
        uppg4();
    }

    private static void uppg1(String exampleWord){
        // Skapa ett Predicate
        // Testköra det i en if-sats
        // Fundera på när det kan vara lämpligt med ett Predicate
        //String exampleWord = "Hejsan";
        Predicate<String> moreThanFiveCharacters = word -> word.length() > 5;
        if(moreThanFiveCharacters.test(exampleWord))
            System.out.println(exampleWord + " har fler än 5 bokstäver");
    }

    private static void uppg2(){
        // Skapa och testkör en Consumer
        String exampleWord = "hej";
        Consumer<String> shoutTheWord = word -> System.out.println(word.toUpperCase().concat("!!!!"));

        shoutTheWord.accept(exampleWord);
    }

    private static void uppg4(){
        // Skapa en klass med namn, ålder, lön
        // Använd comparator för jämförelser / sorteringar
        Comparator<Employee> sortEmpByName = (emp1, emp2) -> emp1.name().compareTo(emp2.name());
        Comparator<Employee> sortEmpBySalary = (emp1, emp2) -> Integer.compare(emp1.salary(), emp2.salary());
        Comparator<Employee> sortEmpByHighestSalaryThenNameThenHighestAge =
                Comparator
                        .comparing(Employee::salary, Comparator.reverseOrder())
                        .thenComparing(Employee::name, String.CASE_INSENSITIVE_ORDER)
                        .thenComparing(Employee::age, Comparator.reverseOrder());

        List<Employee> employeeList = new ArrayList<>(List.of(
                new Employee("Alice", 32000, 24),
                new Employee("Bob", 33000, 30),
                new Employee("Amanda", 33000, 35),
                new Employee("Amanda", 33000, 40)
        ));

        employeeList.sort(sortEmpByHighestSalaryThenNameThenHighestAge);
        System.out.println(employeeList);

    }

    private static void uppg2_metodreferens(){
        // Omvandla följande till metodreferens

        // x -> Integer.sum(x, 2),
        Function<Integer, Integer> sum = x -> Integer.sum(x, 2);

        //x -> Math.sqrt(x),
        Function<Double, Double> sqrt = Math::sqrt;

        //x -> x.toString().toUpperCase();
        Function<Integer, String> intToString = x -> x.toString().toUpperCase();

        // x -> x.length()
        Function<String, Integer> countLettersInWord = String::length;
    }
}

record Employee(String name, int salary, int age){}
