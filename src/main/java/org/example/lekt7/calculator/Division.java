package org.example.lekt7.calculator;

public class Division implements Operation {

    private double a;
    private double b;

    public Division(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double apply() {
        return a / b;
    }
}
