package org.example.lekt7.calculator;


public class Calculations {

    public double calculate(Operation operation){
        return operation.apply();
    }

}
