package org.example.lekt7.calculator;

import java.text.NumberFormat;

public class CalculatorDisplay {

    private final NumberFormat numberFormat;

    public CalculatorDisplay(NumberFormat numberFormat) {
        this.numberFormat = numberFormat;
    }

    public void DisplayResult(double result){
        System.out.println("Result: " + numberFormat.format(result));
    }
}
