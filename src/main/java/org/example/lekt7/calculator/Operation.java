package org.example.lekt7.calculator;

public interface Operation {

    double apply();

}
