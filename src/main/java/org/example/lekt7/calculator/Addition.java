package org.example.lekt7.calculator;

public class Addition implements Operation {

    private int a;
    private int b;

    public Addition(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double apply() {
        return a + b;
    }
}
