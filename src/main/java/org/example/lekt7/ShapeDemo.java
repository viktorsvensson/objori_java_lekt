package org.example.lekt7;

class Rectangle {
    protected int width;
    protected int height;

    public Rectangle(int width, int height){
        this.height = height;
        this.width = width;
    }

    public int getArea(){
        return width * height;
    }

    public void setWidth(int width){
        this.width = width;
    }

    public void setHeight(int height){
        this.height = height;
    }
}

class Square extends Rectangle {
    public Square(int side) {
        super(side, side);
    }
}

public class ShapeDemo {

    public static void main(String[] args) {
        Square square = new Square(10);
        square.setHeight(1);
        System.out.println(square.getArea());
    }

}
