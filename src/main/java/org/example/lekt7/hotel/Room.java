package org.example.lekt7.hotel;

public interface Room {

    void book(String guestName);

}
