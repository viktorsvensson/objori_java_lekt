package org.example.lekt7.hotel;

public class SleepableRoomService extends RoomService{

    private final SleepableRoom sleepableRoom;

    public SleepableRoomService(SleepableRoom hotelRoom) {
        super(hotelRoom);
        this.sleepableRoom = hotelRoom;
    }

    public void orderWakeUp(){
        sleepableRoom.setOrderedWakeUp(true);
    }

}
