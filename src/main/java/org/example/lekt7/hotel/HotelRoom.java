package org.example.lekt7.hotel;

public class HotelRoom implements SleepableRoom {

    protected String guestName;
    protected boolean orderedWakeUp;

    public HotelRoom() {
    }

    @Override
    public void book(String guestName){
        this.guestName = guestName;
    }

    @Override
    public void setOrderedWakeUp(boolean bool){
        this.orderedWakeUp = bool;
    }
}
