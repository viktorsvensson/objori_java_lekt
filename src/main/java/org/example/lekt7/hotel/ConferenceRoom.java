package org.example.lekt7.hotel;

public class ConferenceRoom implements Room{

    private String guestName;

    public ConferenceRoom(String guestName) {
        this.guestName = guestName;
    }

    @Override
    public void book(String guestName) {
        this.guestName = guestName;
    }

}
