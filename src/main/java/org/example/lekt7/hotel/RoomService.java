package org.example.lekt7.hotel;

public class RoomService {

    private Room hotelRoom;

    public RoomService(Room hotelRoom) {
        this.hotelRoom = hotelRoom;
    }

    public void bookInName(String name){
        hotelRoom.book(name);
    }

}
