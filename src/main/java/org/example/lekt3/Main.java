package org.example.lekt3;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Car car = new Car(new SosAlerter());
        Driveable car2 = new Car(new SosAlerter());

        getAway(car);
        Driveable driveable = new Driveable() {
            @Override
            public void drive() {
                System.out.println("Nu kör jag");
            }
        };
        getAway(driveable);
        Driveable driveable2 = Driveable.getDriveAbleWithSound("Woop woop");
        driveable2.drive();

        List<Driveable> driveables = new ArrayList<>();
        driveables.add(car);
        driveables.add(driveable);

        List<Driveable> driveables2 = new ArrayList<>(List.of(car, car2));

        List<Driveable> driveables3 = List.of(car, car2, driveable);
        // Unchecked exeption - UnsupportedOperationException
        //driveables3.add(driveable2);



    }

    public static void getAway(Driveable driveable){
        driveable.drive();
    }
}
