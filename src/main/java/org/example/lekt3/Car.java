package org.example.lekt3;

public class Car implements Driveable {

    private SosAlerter sosAlerter;

    public Car(SosAlerter sosAlerter) {
        this.sosAlerter = sosAlerter;
    }

    @Override
    public void drive() {
        System.out.println("Drive method called");
    }

    public void startCar() {
        try {
            System.out.println("Tidigare steg att utföra");
            sosAlerter.checkConnectivty();
            System.out.println("Fler steg att utföra");
        } catch (NetworkFailureException e){
            System.out.println(e.getMessage());
        }
    }

}
