package org.example.lekt3;

public interface Driveable {

    default void drive(){
        System.out.println("Vroom");
    }

    static Driveable getDriveAbleWithSound(String sound){
        return new Driveable() {
            @Override
            public void drive() {
                System.out.println(sound);
            }
        };
    }

}
