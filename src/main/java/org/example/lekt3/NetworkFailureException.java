package org.example.lekt3;

public class NetworkFailureException extends Exception{

    public NetworkFailureException(String message) {
        super(message);
    }
}
