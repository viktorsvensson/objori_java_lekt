package org.example.lekt1;

public class Main {

    public static void main(String[] args) {

        // Variable
        String name = "Gunnar";

        System.out.println(name);

        int sum = add(5, 10);
        System.out.println(sum);

        Car car1 = new Car("Red", 1993);
        Car car2 = new Car("Blue", 2002);

        System.out.println(car1.getColor());
        System.out.println(car2.getColor());
        car1.setColor("Green");
        System.out.println(car1.getColor());

    }

    public static int add(int a, int b){
        return a + b;
    }

}
