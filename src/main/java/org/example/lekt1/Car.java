package org.example.lekt1;

public class Car {

    private String color;
    private int yearOfCreation;

    public Car(String color, int yearOfCreation){
        this.color = color;
        this.yearOfCreation = yearOfCreation;
    }

    public String getColor(){
        return color;
    }

    public void setColor(String color){
        this.color = color;
    }

}
