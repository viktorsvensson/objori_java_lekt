package org.example.lekt9.metodreferens;

import java.util.Comparator;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

class User {
    private final String name;

    public User(String name) {
        this.name = name;
    }

    public boolean nameMatches(User otherUser){
        return name.equals(otherUser.name);
    }
}

public class Demo {

    public static void main(String[] args) {

        // Statisk metod
        Consumer<Integer> print = num -> System.out.println(num);
        // får ej göras mer än applicera metoden
        Consumer<Integer> printNotRef = num -> System.out.println(num + 1);
        // num -> System.out.println(num)
        // System.out::println
        // ContainingClass::staticMethodName
        Consumer<Integer> printRef = System.out::println;

        Stream.of(1, 2, 3, 4)
                .forEach(System.out::println);

        // Icke-statisk metod, ej knutet till ett specifik objekt
        Function<String, Integer> countWords = word -> word.length();
        // word -> word.length()
        // String::length
        // ContainingType::instanceMethodName
        Function<String, Integer> countWordsRef = String::length;

        Stream.of("Båt", "Hund", "Katt")
                .map(String::length)
                .forEach(System.out::println);

        // Icke-statisk metod, knutet till ett specifik objekt
        User bob1 = new User("Bob");
        User alice = new User("Alice");
        User bob2 = new User("Bob");
        Predicate<User> hasSameNameAsBob1 = user -> bob1.nameMatches(user);
        // user -> bob1.nameMatches(user)
        // bob1::nameMatches
        // variableName::instanceMethodName
        Predicate<User> hasSameNameAsBob1Ref = bob1::nameMatches;

        Stream.of(alice, bob2)
                .filter(user -> bob1.nameMatches(user))
                .filter(bob1::nameMatches) // identisk med filter ovan
                .forEach(System.out::println);

        // Icke-statisk metod, knutet till ett specifik objekt, endast 'qualifier" behövs
        Predicate<Integer> isEven = number -> number % 2 == 0;
        Stream.of(1, 2, 3, 4)
                .filter(num -> isEven.test(num))
                .filter(isEven::test) // identisk beteende med ovan
                .filter(isEven) // identiskt beteende med ovan
                .toList();

        // Anrop till konstruktor
        Function<String, User> nameToUser = name -> new User(name);
        // name -> new User(name);
        // User::new
        // ClassName::new
        Function<String, User> nameToUserRef = User::new;

        Stream.of("Alice", "Bob", "Charlie")
                .map(User::new)
                .toList();

        // Metodrefernser med multipla argument
        // Fungerar det så länge argumentet appliceras i samma ordning
        BiFunction<Integer, Integer, Integer> sum = (x, y) -> Integer.sum(x, y);
        BiFunction<Integer, Integer, Integer> sumRef = Integer::sum;
        // "Fel" ordning kan ej köras som metodreferens
        BiFunction<Integer, Integer, Integer> sumWrongOrder = (x, y) -> Integer.sum(y, x);
        // Ej matchande anrop kan ej köras som metodreferens
        BiFunction<Integer, Integer, Integer> sumWrongOrder2 = (x, y) -> Integer.sum(y, y);

        Comparator<String> matches = (word1, word2) -> word1.compareTo(word2);
        Comparator<String> matchesRef = String::compareTo;

        // Ambigous when we go from method reference back to which method?
        Function<Integer, String> intToString = num -> num.toString();
        Function<Integer, String> intToString2 = num -> Integer.toString(num);

        // Funkar ej
        //Function<Integer, String> intToStringRef = Integer::toString;

        // Kanske något överraskande från num -> num.toString()
        Function<Integer, String> intToStringRef2 = Object::toString;

        Integer number = 5;
        String result = number.toString() + number.toString();
        System.out.println(result);
    }
}
