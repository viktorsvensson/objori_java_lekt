package org.example.lekt9.impvsstream;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<Employee> employees = List.of(
                new Employee("Alice", 34000),
                new Employee("Bob", 29000),
                new Employee("Charlie", 36000)
        );

        // Imperativ
        // filtrera ut anställda med mer än 30'000
        List<Employee> wellPaidEmployees = new ArrayList<>();
        for(int i = 0; i < employees.size(); i++){
            Employee employee = employees.get(i);
            if(employee.getSalary() > 30000){
                wellPaidEmployees.add(employee);
            }
        }

        // Sortera listan efter lön (Bubble sort)
        boolean sorted = false;
        Employee temp;
        while(!sorted){
            sorted = true;
            for(int i = 0; i < wellPaidEmployees.size() - 1; i++){
                if(wellPaidEmployees.get(i).getSalary() < wellPaidEmployees.get(i + 1).getSalary()){
                    temp = wellPaidEmployees.get(i);
                    wellPaidEmployees.set(1, wellPaidEmployees.get(i + 1));
                    wellPaidEmployees.set(i + 1, temp);
                    sorted = false;
                }
            }
        }

        // Print out
        for(int i = 0; i < wellPaidEmployees.size(); i++){
            Employee employee = employees.get(i);
            System.out.println(employee);
        }

        // Streams - funktionellt & deklarativt-ish
        employees
                .stream()
                .filter(employee -> employee.getSalary() >= 30000)
                .sorted(Comparator.comparing(Employee::getSalary).reversed())
                .forEach(System.out::println);

    }
}
