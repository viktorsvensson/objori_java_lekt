package org.example.lekt9.functionalinterface;

import java.util.ArrayList;
import java.util.List;

@FunctionalInterface
interface Predicatable {
    boolean check(String word);
}

public class Main {

    public static void main(String[] args) {

        List<String> strings = List.of("Alice", "Amanda", "Bob", "Charlie");

        Predicatable startsWithA = new Predicatable() {
            @Override
            public boolean check(String word) {
                return word.startsWith("A");
            }
        };

        Predicatable startsWithALambda = word -> word.startsWith("A");
        startsWithALambda.check("Gunnar");
        Predicatable endsWithBFullLambda = (word) -> {
            boolean result = word.endsWith("B");
            return result;
        };

        Predicatable moreThanThreeCharacters = new Predicatable() {
            @Override
            public boolean check(String word) {
                return word.length() > 3;
            }
        };

        List<String> result = filtering(strings, moreThanThreeCharacters);
        List<String> result2 = filtering(strings, word -> word.endsWith("b"));
        System.out.println(result2);

    }

    private static List<String> filtering(
                List<String> strings,
                Predicatable predicatable
                ){
            List<String> filteredList = new ArrayList<>();
            for(String word : strings){
                if(predicatable.check(word)){
                    filteredList.add(word);
            }
        }
        return filteredList;
    }



}
