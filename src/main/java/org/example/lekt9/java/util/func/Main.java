package org.example.lekt9.java.util.func;

import org.example.lekt9.impvsstream.Employee;

import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        // Function<ParamTyp, ReturTyp>, transformerar / modifierar värden
        // Räknar antalet bokstäver i ett ord
        Function<String, Integer> countCharacter = word -> word.length();
        int result = countCharacter.apply("Hund");
        System.out.println(result);

        // Predicate<ParamTyp>, returnerar true / false om villkor uppfyllt
        Predicate<Integer> isEven = x -> x % 2 == 0;
        boolean predicateResult = isEven.test(4);
        System.out.println(predicateResult);

        // Consumer<ParamTyp>, "konsumerar" data, returnerar inget
        Consumer<String> printOutValue = x -> System.out.println(x);
        printOutValue.accept("Hello World!");

        // Comparator<ParamTyp>, jämför två objekt av samma typ
        Comparator<Integer> lowestValue = (val1, val2) -> Integer.compare(val1, val2);
        int diff = lowestValue.compare(10, 11);
        System.out.println("Differensen var: " + diff);

        List<Integer> sortedNumbers = Stream.of(-250012312, 250012312)
                .sorted(lowestValue)
                .toList();

        System.out.println(sortedNumbers);

        Comparator<String> alphabetical = (word1, word2) -> word1.compareTo(word2);
        List<String> names = Stream.of("Alice", "Bob", "Amanda", "Charlie")
                .sorted(alphabetical)
                .toList();
        System.out.println(names);

        List<Employee> employees = Stream.of(
                new Employee("Alice", 31000),
                new Employee("Bob", 35000),
                new Employee("Eva", 35000)
                )
                .sorted(Comparator.comparing((Employee emp) -> emp.getSalary())
                        .thenComparing((Employee emp) -> emp.getName()))
                .toList();

        System.out.println(employees);

    }

}
