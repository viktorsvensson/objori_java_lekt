package org.example.lekt8;

import java.util.ArrayList;
import java.util.List;

interface Observer {
    void update(String story);
}

class NormalNewsPaper implements Observer {

    @Override
    public void update(String story) {
        printNews(story);
    }

    private void printNews(String story){
        System.out.println(
                "Newsflash från dagliganyheter: "
                        + story
        );
    }
}

class ShoutyNewsPaper implements Observer {

    @Override
    public void update(String story) {
        printNews(story);
    }

    private void printNews(String story){
        System.out.println("Newsflash från ShoutyNews: "
            + story.toUpperCase() + "!!!!!");
    }

}

class FreeLancerJournalist {

    private String latestStory;
    List<Observer> observers = new ArrayList<>();

    public void addObserver(Observer observer){
        observers.add(observer);
    }

    public void setLatestStory(String story) {
        this.latestStory = story;
    }

    public void notifyObservers(){
        for(Observer observer : observers){
            observer.update(latestStory);
        }
    }
}

public class Main {

    public static void main(String[] args) {

        FreeLancerJournalist journalist = new FreeLancerJournalist();
        NormalNewsPaper normalNewsPaper = new NormalNewsPaper();
        ShoutyNewsPaper shoutyNewsPaper = new ShoutyNewsPaper();

        journalist.addObserver(normalNewsPaper);
        journalist.addObserver(shoutyNewsPaper);

        journalist.setLatestStory("Brödpriserna stiger");
        journalist.notifyObservers();


    }

}
